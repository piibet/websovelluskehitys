﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websovellukset_ASP_NET_Web_Application.Models;

namespace Websovellukset_ASP_NET_Web_Application.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            UserData model = new UserData()
            {
                Age = 22,
                Name = "Teppo "
            };

            return View(model);
        }

        public ActionResult Contact(string id, int? age, string info)
        {
            ViewBag.Message = info;

            UserData model = new UserData()
            {
                Age = age.GetValueOrDefault(),
                Name = id
            };

            return View(model);
        }
    }
}