﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websovellukset_ASP_NET_Web_Application.Models;

namespace Websovellukset_ASP_NET_Web_Application.Controllers
{
    public class UserInfoController : Controller
    {

        // GET: UserInfo/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UserInfo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserInfo/Create
        [HttpPost]
        public ActionResult Create(UserData model)
        {
            Debug.WriteLine($"Data oli. Nimi: {model.Age} ja sposti {model.Email}");

            return View(model);
        }

    }
}
