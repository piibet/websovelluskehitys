﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Websovellukset_ASP_NET_Web_Application.Models
{
    public class UserData
    {
        public string Name { get; set; }
        public int Age { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}